# Android Pong
Two Androids enter the Pong Arena. Only one will survive!

Android Support on Devices running Android 5.0 or later(A preview of the game running on a Samsung Galaxy S7 Phone with Android 7.0)
![android pong](https://user-images.githubusercontent.com/18353476/28750449-2f2766f4-74a0-11e7-9801-76d17154c390.PNG)

# Unity3d(Building for PC, Mac, and Android)
https://unity3d.com/

![build_to_android_5](https://user-images.githubusercontent.com/18353476/27527819-55779986-5a02-11e7-96cc-bfaeb3a1b5f6.png)
![playersetandroidconfiguration](https://user-images.githubusercontent.com/18353476/28398802-b888561e-6cbd-11e7-9bd4-9d77f33e424e.png)
![playersetandroidpublish](https://user-images.githubusercontent.com/18353476/28398805-ba6209d0-6cbd-11e7-87e4-c1ce57973303.png)

# Getting started with Android development

https://docs.unity3d.com/Manual/android-GettingStarted.html

# Android Studio
https://developer.android.com/studio/index.html

A good tutorial for Android Studio Setup(Windows, Mac, and Linux): https://www.tutorialspoint.com/android/android_studio.htm

![as](https://user-images.githubusercontent.com/18353476/28494127-6da78c40-6eda-11e7-8fa0-d77a5294b193.png)
